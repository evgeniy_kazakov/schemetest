(RESTART 1)
(define nil '())

(define (call-each procedures)
  (if (null? procedures)
      'done
      (begin
        ((car procedures))
        (call-each (cdr procedures)))))

;Queue
    (define (front-ptr queue) (car queue))
    (define (rear-ptr queue) (cdr queue))
    (define (set-front-ptr! queue item) (set-car! queue item))
    (define (set-rear-ptr! queue item) (set-cdr! queue item))

    (define (empty-queue? queue)
      (null? (front-ptr queue)))

    (define (make-queue)
      (cons '() '()))

    (define (front-queue queue)
      (if (empty-queue? queue)
          (error "FRONT called with an empty queue" queue)
          (car (front-ptr queue))))

    (define (insert-queue! queue item)
      (let ((new-pair (cons item '())))
          (cond ((empty-queue? queue)
                  (set-front-ptr! queue new-pair)
                  (set-rear-ptr! queue new-pair)
                  queue)
                (else
                  (set-cdr! (rear-ptr queue) new-pair)
                  (set-rear-ptr! queue new-pair)
                  queue))))

    (define (delete-queue! queue)
      (cond ((empty-queue? queue)
             (error "DELETE! called with an empty queue" queue))
            (else
             (set-front-ptr! queue (cdr (front-ptr queue)))
             queue)))

    (define (print-queue queue)
      (display "queue: ")
      (display (front-ptr queue)))


(define (make-wire)
  (let ((signal-value 0) (action-procedures '()))
    (define (set-my-signal! new-value)
      (if (not (= signal-value new-value))
          (begin (set! signal-value new-value)
                 (call-each action-procedures))
          'done))
    (define (accept-action-procedure! proc)
      (set! action-procedures (cons proc action-procedures))
      (proc))

    (define (dispatch m)
      (cond ((eq? m 'get-signal) signal-value)
            ((eq? m 'set-signal!) set-my-signal!)
            ((eq? m 'add-action!) accept-action-procedure!)
            (else (error "Unknown operation -- WIRE" m)))
      )

    dispatch))

(define (call-each procedures)
  (if (null? procedures)
      'done
      (begin
        ((car procedures))
        (call-each (cdr procedures)))))


(define (get-signal wire) (wire 'get-signal))
(define (set-signal! wire new-value) ((wire 'set-signal!) new-value))
(define (add-action! wire action-procedure) ((wire 'add-action!) action-procedure))

(define (logical-not s)
  (cond ((= s 0) 1)
        ((= s 1) 0)
        (else (error "Invalid signal" s))))

(define (logical-and a b)
  (cond ((and (= a 1) (= b 1)) 1)
        (else 0)))

(define (logical-or a b)
  (cond ((and (= a 0) (= b 0)) 0)
        (else 1)))


(define (inverter input output)
  (define (invert-input)
    (let ((new-value (logical-not (get-signal input))))
        (after-delay inverter-delay
                     (lambda ()
                       (set-signal! output new-value)))))
  (add-action! input invert-input)
  'ok)

(define (and-gate a1 a2 output)
  (define (and-action-procedure)
    (let ((new-value
            (logical-and (get-signal a1) (get-signal a2))))
        (after-delay and-gate-delay
                     (lambda ()
                       (set-signal! output new-value)))))

  (add-action! a1 and-action-procedure)
  (add-action! a2 and-action-procedure)
  'ok)

(define (or-gate a1 a2 output)
  (define (or-action-proc)
    (let ((new-value
            (logical-or (get-signal a1) (get-signal a2))))
        (after-delay or-gate-delay
                     (lambda ()
                       (set-signal! output new-value)))))
  (add-action! a1 or-action-proc)
  (add-action! a2 or-action-proc)
  'ok)

(define (half-adder a b s c)
  (let ((d (make-wire)) (e (make-wire)))
    (or-gate a b d)
    (and-gate a b c)
    (inverter c e)
    (and-gate d e s)
    'ok))

(define (full-adder a b c-in sum c-out)
  (let ((s (make-wire))
        (c1 (make-wire))
        (c2 (make-wire)))
    (half-adder b c-in s c1)
    (half-adder a s sum c2)
    (or-gate c1 c2 c-out)
    'ok))

(define (probe name wire)
  (add-action! wire
               (lambda ()
                 (newline)
                 (display name)
                 (display " ")
                 (display (current-time the-agenda))
                 (display "  New-value = ")
                 (display (get-signal wire)))))

(define (make-time-segment time queue) (cons time queue))
(define (segment-time s) (car s))
(define (segment-queue s) (cdr s))

;Agenda

(define (make-agenda) (list 0))

(define (current-time agenda) (car agenda))
(define (set-current-time! agenda time) (set-car! agenda time))

(define (segments agenda) (cdr agenda))
(define (set-segments! agenda segments) (set-cdr! agenda segments))

(define (first-segnent agenda) (car (segments agenda)))
(define (rest-segnent agenda) (cdr (segments agenda)))

(define (empty-agenda? agenda) (null? (segments agenda)))

(define (add-to-agenda! time action agenda)
    (define (belongs-before? segments)
        (or (null? segments)
            (< time (segment-time (car segments)))))

    (define (make-new-timesegment timme action)
      (let ((q (make-queue)))
        (insert-queue! q action)
        (make-time-segment time q)))

    (define (add-to-segments! segments)
      (if (= (segment-time (car segments)) time)
          (insert-queue! (segment-queue (car segments)) action)
          (let ((rest (cdr segments)))
            (if (belongs-before? rest)
                (set-cdr! segments
                          (cons (make-new-timesegment time action)
                                (cdr segments)))
                (add-to-segments! rest)))))

    (let ((segments (segments agenda)))
        (if (belongs-before? segments)
            (set-segments! agenda
                           (cons (make-new-timesegment time action)
                                 segments))
            (add-to-segments! segments))))

(define (remove-first-agenda-item! agenda)
  (let ((q (segment-queue (first-segnent agenda))))
    (delete-queue! q)
    (if (empty-queue? q)
        (set-segments! agenda (rest-segnent agenda)))))

(define (first-agenda-item agenda)
  (if (empty-agenda? agenda)
      (error "Agenda is empty –– FIRST-AGENDA-ITEM")
      (let ((fitst-seg (first-segnent agenda)))
        (set-current-time! agenda (segment-time fitst-seg))
        (front-queue (segment-queue fitst-seg)))))

(define (after-delay delay action)
  (add-to-agenda! (+ delay (current-time the-agenda))
                  action
                  the-agenda))

(define (propagate)
  (if (empty-agenda? the-agenda)
      'done
      (let ((fitst-item (first-agenda-item the-agenda)))
          (fitst-item)
          (remove-first-agenda-item! the-agenda)
          (propagate)
          )
      )
  )

(define the-agenda (make-agenda))
(define inverter-delay 2)
(define and-gate-delay 3)
(define or-gate-delay 5)

(define (ripple-carry-adder A B S c-in)
  (define c-out (make-wire))
  (full-adder (car A) (car B) c-in (car S) c-out)
  (if (not (null? (cdr A)))
      (ripple-carry-adder (cdr A) (cdr B) (cdr S) c-out)
      c-out))

(define a0 (make-wire))
(define a1 (make-wire))
(define a2 (make-wire))

(define b0 (make-wire))
(define b1 (make-wire))
(define b2 (make-wire))

(define s0 (make-wire))
(define s1 (make-wire))
(define s2 (make-wire))

(define A (list a0 a1 a2))
(define B (list b0 b1 b2))
(define S (list s0 s1 s2))
(define c-in (make-wire))

(probe 'sum0 s0)
(probe 'sum1 s1)
(probe 'sum2 s2)
(probe 'cin c-in)

(ripple-carry-adder A B S c-in)
(set-signal! a0 1)
(set-signal! b0 0)
(set-signal! a1 1)
(propagate)

;(define input-1 (make-wire))
;(define input-2 (make-wire))
;(define sum (make-wire))
;(define carry (make-wire))
;(probe 'sum sum)
;(probe 'carry carry)

;(half-adder input-1 input-2 sum carry)
;(set-signal! input-1 1)
;(propagate)

;(set-signal! input-2 1)






