(RESTART 1)

(define nil '())
(define (front-ptr queue) (car queue))
(define (rear-ptr queue) (cdr queue))
(define (set-front-ptr! queue item) (set-car! queue item))
(define (set-rear-ptr! queue item) (set-cdr! queue item))

(define (empty-deque? queue)
  (and (null? (front-ptr queue))
       (null? (rear-ptr queue))))

(define (make-deque) (cons nil nil))

(define (front-deque queue)
  (if (empty-deque? queue)
      (error "FRONT called with an empty queue" queue)
      (car (front-ptr queue))))

(define (front-insert-deque! queue item)
  (let ((new-item (make-item item)))
      (cond ((empty-deque? queue)
             (set-front-ptr! queue new-item)
             (set-rear-ptr! queue new-item))
            (else
              (set-item-next! new-item (front-ptr queue))
              (set-item-prev! (front-ptr queue) new-item)
              (set-front-ptr! queue new-item)))))

(define (rear-insert-deque! queue item)
  (let ((new-item (make-item item)))
    (cond ((empty-deque? queue)
           (set-front-ptr! queue new-item)
           (set-rear-ptr! queue new-item))
          (else
           (set-item-prev! new-item (rear-ptr queue))
           (set-item-next! (rear-ptr queue) new-item)
           (set-rear-ptr! queue new-item)))))

(define (front-delete-deque! queue)
  (cond ((empty-deque? queue)
         (error "DELETE! called with an empty queue" queue))
        (else
         (set-front-ptr! queue (cdr (front-ptr queue)))
         queue)))

(define (rear-delete-deque! queue item)
  (cond ((empty-deque? queue)
         (error "DELETE! called with an empty queue" queue))
        (else
          ())))

(define (print-deque deque)
  (display "deque:")
  (define (print-iter item)
    (display " ")
    (display (item-value item))
    (cond ((null? (item-next-ptr item))
           (display ";"))
          (else
            (print-iter (item-next-ptr item)))))
  (print-iter (front-ptr deque)))



(define (make-item value)
  (cons (cons value nil) nil))

(define (item-prev-ptr item)
  (cdar item))

(define (item-next-ptr item)
  (cdr item))

(define (item-value item)
  (caar item))

(define (set-item-next! item next-item)
  (set-cdr! item next-item))

(define (set-item-prev! item prev-item)
  (set-cdr! (car item) prev-item))

(define q1 (make-deque))

(rear-insert-deque! q1 'a)
(rear-insert-deque! q1 'b)
(front-insert-deque! q1 'v)
(front-insert-deque! q1 'c)
(front-insert-deque! q1 'e)
(print-deque q1)

;(define l1 (make-item 'a))
;(define l2 (make-item 'b))
;(define l3 (make-item 'c))


;(set-item-next! l1 l2)
;(set-item-prev! l2 l1)
;(set-item-next! l2 l3)
;(set-item-prev! l3 l2)

;(print-deque l1)
;(set-cdr! (car l2) l1)



