

(define (make-mobile left right)
  (list left right))

(define (make-branch length srtucture)
  (list length srtucture))

(define (left-branch mobile)
  (car mobile))

(define (right-branch mobile)
  (car (cdr mobile)))

(define (branch-length branch)
  (car branch))

(define (branch-structure branch)
  (car (cdr branch)))

;(define mobile-0 (make-mobile (make-branch 1 2) (make-branch 3 4)))
;(define mobile-1 (make-mobile (make-branch 1 mobile-0) (make-branch 3 5)))

(define (branch-weight branch)
  (let ((struct (branch-structure branch)))
    (cond ((not (pair? struct)) struct)
           (else (+ (branch-weight (left-branch struct))
                    (branch-weight (right-branch struct)))))
      ))

(define (total-weight mobile)
  (+ (branch-weight (left-branch mobile)) (branch-weight (right-branch mobile))))

(define (branch-torque branch)
  (* (branch-length branch) (branch-weight branch)))



(define (balanced? mobile)
  (define (sub-balanced? branch)
      (let ((struct (branch-structure branch)))
          (if (not (pair? struct))
              true
              (balanced? struct))))

  (and (= (branch-torque (left-branch mobile)) (branch-torque (right-branch mobile)))
       (sub-balanced? (left-branch mobile))
       (sub-balanced? (right-branch mobile))))


(define mobile-l0 (make-mobile (make-branch 3 2) (make-branch 2 3)))
(define mobile-r0 (make-mobile (make-branch 2 3) (make-branch 3 2)))
(define mobile-1 (make-mobile (make-branch 3 mobile-l0)
                              (make-branch 3 mobile-r0)))

(total-weight mobile-1)
(balanced? mobile-1)

;(define (total-weight mobile)
;  (define (iter l-branch r-branch result)
;    (let ((mobile? (pair? (branch-structure r-branch)))
;          (sub-mobile (branch-structure)))
;        (+ (if mobile?)
;           (iter )
;           (branch-structure r-branch))))
;  (iter (left-branch mobile) (right-branch mobile) 0))