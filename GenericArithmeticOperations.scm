 (define (tag x)
    (attach-tag 'scheme-number x))

 (define (attach-tag type-tag contents)
  (cons type-tag contents))

((lambda (x) (tag x)) 1)
