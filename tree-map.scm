(define nil '())

(define (squre x)
  (* x x))

(define (square-tree-1 tree)
  (cond ((null? tree) nil)
        ((not (pair? tree)) (squre tree))
        (else (cons (square-tree (car tree))
                    (square-tree (cdr tree))))))

(define (square-tree-2 tree)
  (map (lambda (sub-tree) (if (pair? sub-tree)
                               (square-tree sub-tree)
                               (squre sub-tree)))
       tree))

(define (tree-map proc tree)
  (map (lambda (sub-tree) (if (pair? sub-tree)
         (tree-map proc sub-tree)
         (proc sub-tree)))
       tree))

(define (square-tree tree)
  (tree-map squre tree))

(square-tree
 (list 1
       (list 2 (list 3 4) 5)
       (list 6 7)))


(define (magic s)
  (lambda (x) (append s x)))

(define (subsets s)
  (if (null? s)
      (list nil)
      (let ((rest (subsets (cdr s))))
        (append rest (map (magic (list (car s))) rest)))))

(subsets (list 1 2 3))

;(define rest-0 (append (list nil) (map (magic (list 3)) (list nil))))
;(define rest-1 (append rest-0 (map (magic (list 2)) rest-0)))
;(define rest-2 (append rest-1 (map (magic (list 1)) rest-1)))
;rest-0
;rest-1
;rest-2










