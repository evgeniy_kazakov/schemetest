(define (make-point x y)
  (cons x y))

(define (x-point point)
  (car point))

(define (y-point point)
  (cdr point))

(define (make-segment start end)
  (cons start end))

(define (start-segment segment)
  (car segment))

(define (end-segment segment)
  (cdr segment))

(define (midpoint-segment segment)
  (let ((x1 (x-point (start-segment segment)))
        (x2 (x-point (end-segment segment)))
        (y1 (y-point (start-segment segment)))
        (y2 (y-point (end-segment segment))))
      (make-point (/ (+ x2 x1) 2) (/ (+ y2 y1) 2))))


(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

(define (print-segment segment)
  (define (print-p p)
    (display "(")
    (display (x-point p))
    (display ",")
    (display (y-point p))
    (display ")"))

  (newline)
  (display "{")
  (print-p (start-segment segment))
  (display ",")
  (print-p (end-segment segment))
  (display "}"))

(define (make-rect top-left bottom-right)
  (cons top-left bottom-right))

(define (rect-height rect)
  (let ((y1 (y-point (car rect)))
        (y2 (y-point (cdr rect))))
      (- y2 y1)))

(define (rect-width rect)
  (let ((x1 (x-point (car rect)))
        (x2 (x-point (cdr rect))))
      (- x2 x1)))

(define (rect-area rect)
  (* (rect-width rect) (rect-height rect)))

(define (rect-perimeter rect)
  (+ (* 2 (rect-width rect)) (* 2 (rect-height rect))))

(define A (make-point 2 3))
(define B (make-point 4 5))

(define R (make-rect A B))

(rect-area R)
(rect-perimeter R)

;(define AB (make-segment A B))