(define nil '())


(define (filter predicate sequence)
  (cond ((null? sequence) nil)
        ((predicate (car sequence))
         (cons (car sequence)
               (filter predicate (cdr sequence))))
        (else (filter predicate (cdr sequence)))))

(define (accumulate op initial sequence)
  (cond ((null? sequence) initial)
        (else (op (car sequence)
                  (accumulate op initial (cdr sequence))))))

(define (enumerate-interval low high)
  (if (> low high)
      nil
      (cons low (enumerate-interval (+ low 1) high))))

(define (enumerate-tree tree)
  (cond ((null? tree) nil)
        ((not (pair? tree)) (list tree))
        (else (append (enumerate-tree (car tree))
                      (enumerate-tree (cdr tree))))))

(filter odd? (list 1 2 3 4 5))
(accumulate + 0 (list 1 2 3 4 5))
(accumulate * 1 (list 1 2 3 4 5))
(enumerate-interval 2 7)
(enumerate-tree (list 1 (list 2 (list 3 4)) 5))

(define (map p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) nil sequence))

(map (lambda (x) (+ x 1)) (list 1 2 3 4 5 6))

(define (append seq1 seq2)
  (accumulate cons seq2 seq1))

(append (list 1 2 3 4) (list 5 6 7 8))

(define (length sequence)
  (accumulate (lambda (x y) (+ y 1)) 0 sequence))

(length (list 1 2 3 4))


(define (horner-eval x coefficient-sequence)
  (accumulate (lambda (this-coeff higher-terms) (+ this-coeff (* x higher-terms)))
              0
              coefficient-sequence))

(horner-eval 2 (list 1 3 0 5 0 1))

(define (count-leaves x)
  (cond ((null? x) 0)
        ((not (pair? x)) 1)
        (else (+ (count-leaves (car x))
                 (count-leaves (cdr x))))))

(define (magic tree)
  (if (not (pair? tree))
      1
      (map magic tree)))

(define (op item result)
  (cond ((null? item) result)
        ((not (pair? item)) (+ item result))
        (else (+ (op (car item) result)
                 (op (cdr item) 0)))))


(map magic (list 1 (list 2 (list 3 4)) 5))

(define (count-leaves-acc tree)
  (accumulate op 0 (map magic tree)))

(count-leaves-acc (list 1 (list 2 (list 3 4)) 5))
(count-leaves-acc (list 1 2 3 (list 4 5 (list 6 7))))

(define (accumulate op initial sequence)
  (cond ((null? sequence) initial)
        (else (op (car sequence)
                  (accumulate op initial (cdr sequence))))))


;(accumulate op 0 (1 (1 (1 1)) 1))
;(op 1 (accumulate op 0 ((1 (1 1)) 1)))
;(op 1 (op (1 (1 1)) (accumulate op 0 (1))))
;(op 1 (op (list 1 (list 1 1)) (op 1 0)))
;((1  2  3)
; (4  5  6)
; (7  8  9)
; (10 11 12))
; 22  26 30
(define ss (list (list 1 2 3) (list 4 5 6) (list 7 8 9) (list 10 11 12)))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      nil
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

(accumulate-n + 0 ss)
