(RESTART 1)

(define (front-ptr queue) (car queue))
(define (rear-ptr queue) (cdr queue))
(define (set-front-ptr! queue item) (set-car! queue item))
(define (set-rear-ptr! queue item) (set-cdr! queue item))

(define (empty-queue? queue)
  (null? (front-ptr queue)))

(define (make-queue)
  (cons '() '()))

(define (front-queue queue)
  (if (empty-queue? queue)
      (error "FRONT called with an empty queue" queue)
      (car (front-ptr queue))))

(define (insert-queue! queue item)
  (let ((new-pair (cons item '())))
      (cond ((empty-queue? queue)
              (set-front-ptr! queue new-pair)
              (set-rear-ptr! queue new-pair)
              queue)
            (else
              (set-cdr! (rear-ptr queue) new-pair)
              (set-rear-ptr! queue new-pair)
              queue))))

(define (delete-queue! queue)
  (cond ((empty-queue? queue)
         (error "DELETE! called with an empty queue" queue))
        (else
         (set-front-ptr! queue (cdr (front-ptr queue)))
         queue)))

(define (print-queue queue)
  (display "queue: ")
  (display (front-ptr queue)))


(define (make-queue2)
   (let ((front-ptr '())
         (rear-ptr '()))
         (define (emtpy-queue?) (null? front-ptr))
         (define (set-front-ptr! item) (set! front-ptr item))
         (define (set-rear-ptr! item) (set! rear-ptr item))
         (define (front-queue)
           (if (emtpy-queue?)
             (error "FRONT called with an empty queue")
             (car front-ptr)))
         (define (insert-queue! item)
           (let ((new-pair (cons item '())))
             (cond ((emtpy-queue?)
                     (set-front-ptr! new-pair)
                     (set-rear-ptr! new-pair))
                   (else
                     (set-cdr! rear-ptr new-pair)
                     (set-rear-ptr! new-pair)))))
         (define (delete-queue!)
           (cond ((emtpy-queue?)
                   (error "DELETE called with an emtpy queue"))
                 (else (set-front-ptr! (cdr front-ptr)))))

         (define (print-queue) front-ptr)

         (define (dispatch m)
           (cond ((eq? m 'empty-queue) emtpy-queue?)
                 ((eq? m 'front-queue) front-queue)
                 ((eq? m 'insert-queue!) insert-queue!)
                 ((eq? m 'delete-queue!) delete-queue!)
                 ((eq? m 'print-queue) print-queue)
                 (else (error "undefined operation -- QUEUE" m))))
         dispatch))

(define q1 (make-queue))
(insert-queue! q1 'a)
(insert-queue! q1 'b)

(insert-queue! q1 'c)
(insert-queue! q1 'd)
(print-queue q1)
(delete-queue! q1)
(print-queue q1)
(delete-queue! q1)
(print-queue q1)