(define (make-cycle x)
  (set-cdr! (last-pair x) x)
  x)

(define z (make-cycle (list 'a 'b 'c)))
(define l (list 'a 'b 'c 'd))

(define (detect-cycle x)

  (define (iter a b)
    (cond ((or (not (pair? (cdr a))) (not (pair? (cdr b)))) #f)
          ((eq? (car a) (car b)) #t)
          (else (iter (cdr a) (cddr b)))))

  (iter x (cdr x)))

;(detect-cycle z)
;(detect-cycle l)